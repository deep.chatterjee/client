.. Copyright (C) Alexander Pace (2021)
.. Copyright (C) Duncan Meacher (2021)

igwn-alert Documentation
============================

igwn-alert is a client for the LIGO/Virgo/KAGRA alert infrastructure 
that is powered by `Apache Kafka`_. igwn-alert uses a publish-subcsribe 
(pubsub) model to alert users and follow-up processes to state 
changes in GraceDB. It is a high-level extension of the generalized
`hop-client`_ pubsub tool intended as a replacement of the legacy XMPP
`LVAlert`_ service.

User Guide
----------

Please visit the `igwn-alert User Guide`_ for a brief overview and starter for the
igwn-alert client code and service. 

Existing LVAlert Users are encouraged to read the
`LVAlert to igwn-alert Transition Guide <lvalert_to_igwnalert.html>`_ to learn 
the latest on the rollout schedule and to see the differences between
LVAlert and igwn-alert.

Quick Start
-----------

The latest version of igwn-alert is available in the `igwn-py38`_ 
and `igwn-py39`_ conda environments. 

The latest version is also available on pip_::

    pip install igwn-alert

For custom conda environments where `conda-forge`_ is enabled::

    conda install igwn-alert

Next, see the `User Guide on topics and credentials`_ for instructions on how to 
enable your account and credentials. 

Finally, listen for messages::

    igwn-alert -g gracedb-playground listen

Help
----

Users with questions can email computing-help@igwn.org. LVK members can
reach out for support on the `igwn-alert Mattermost channel`_.

API
---

.. automodule:: igwn_alert.client
   :members:

Command Line Interface
----------------------

.. argparse::
    :module: igwn_alert.tool
    :func: parser
.. _Apache Kafka: https://kafka.apache.org/
.. _hop-client: https://hop-client.readthedocs.io/
.. _pip: https://pypi.org/project/igwn-alert/
.. _conda-forge: https://github.com/conda-forge/igwn-alert-feedstock
.. _User Guide on topics and credentials: guide.html#managing-credentials-and-topics
.. _igwn-alert User Guide: guide.html
.. _LVAlert: https://ligo-lvalert.readthedocs.io/en/latest/
.. _igwn-py38: https://computing.docs.ligo.org/conda/environments/igwn-py38/
.. _igwn-py39: https://computing.docs.ligo.org/conda/environments/igwn-py39/
.. _computing-help@igwn.org:: mailto:computing-help@igwn.org
.. _igwn-alert Mattermost channel: https://chat.ligo.org/ligo/channels/igwn-alert
