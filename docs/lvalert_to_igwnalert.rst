LVAlert to igwn-alert Transition
================================

This page will be updated as need be ahead of O4. 

This page is intended as a brief guide to assist users in adjusting their 
workflows from LVAlert to the new igwn-alert system. 

Rollout Schedule
****************

The legacy LVAlert XMPP notification service will be decommissioned before O4.

Before then, ``gracedb-test.ligo.org`` and ``gracedb-playground.ligo.org`` have been
configured to simultaneously send LVAlerts and igwn-alerts. The message content and 
scenarios (new events/superevents, new log messages, etc.) are the same. This is to 
allow users to test their existing workflows using LVALert and igwn-alert while 
participating in the MDC campaigns on ``gracedb-playground``. Please see the 
schedule below.

.. image:: _static/release_timeline.png
  :alt: igwn_alert release schedule

To emphasize:

* igwn-alerts are currently being sent on the playground and test servers, and 
  will continue to leaving up to O4. 
* The first pre-O4 Engineering Run is nominally scheduled for 
  `mid-November 2022 <https://dcc.ligo.org/DocDB/0180/G2200146/001/Operations%20Reports%20for%202022.pdf>`_
* The production ``gracedb.ligo.org`` server will publish igwn-alerts starting
  in August 2022 (exact date TBD, but nominally the first maintenance Tuesday
  in August)
* On this date, the legacy LVAlert servers will be decommissioned and users
  **will no longer be able to run their LVAlert workflows**.


What's the Same
***************

With some exceptions (to be described below), the workflow for users of
ligo-lvalert's Python API should be largely the same. The `listen <index.html#igwn_alert.client.client.listen>`_ 
method takes in a ``callback`` function  argument and (optionally) a topic or list of topics, and will 
launch the ``callback`` function when a new alert arrives to one of the provided topics. 

Optionally, like the ligo-lvalert tool, a user can receive alerts from the command
line. For example, assuming the user's subscriptions/permissions are set, the following
commands are equivalent::

    lvalert -s lvalert-playground.cgca.uwm.edu listen 

    igwn-alert -g gracedb-playground listen

What's Different
****************

While LVAlert and igwn-alert are both Publish-Subscribe (pubsub) tools, there
are differences between the two tools. These differences are largely semantic 
and should become more more clear by comparing the following table: 

+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
|                            | LVAlert                                                                         | igwn-alert                                                                 |
+============================+=================================================================================+============================================================================+
| Servers                    | There are multiple LVAlert servers. Each one corresponds                        | There is a single SCIMMA server to handle each instance of GraceDB. Users  |
|                            | to an instance of GraceDB (production, playground, test).                       | make a single account, and then finer-grained control is handled by        |
|                            |                                                                                 | account credentials.                                                       |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
| Accounts                   | A user makes an account on each of the LVAlert servers.                         | A user makes a **credential** after gaining access to SCIMMA's server.     |
|                            | The username is tied to their LVK account, and has a                            | The username is tied to their LVK username (plus a random hex string),     |
|                            | customizable password.                                                          | and the password is assigned at credential creation. A user can optionally |
|                            |                                                                                 | create multiple credentials to selectively control what processes receive  |
|                            |                                                                                 | what alerts.                                                               |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
| Alerts are                 | Nodes                                                                           | Topics                                                                     |
| sent to                    |                                                                                 |                                                                            |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
| Alerts are                 | Subscriptions. A user subscribes to a node. Alerts will                         | Permissions. Assigning a "read" **permission** to a topic is analogous     |
| controlled by              | arrive only to nodes for which the user has a subscription.                     | to adding a subscription to a node. Further on permissions: as was         |
|                            |                                                                                 | described earlier, users only have read permission to GraceDB topics.      |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
| Listening to GraceDB       | A separate LVAlert server is available for each GraceDB instance.               | There is a single server, but different **groups** for each instance of    |
| instances                  | The node names (like ``cbc_gstlal``) are identical across servers.              | GraceDB. The topic list is identical across groups, and topic names take   |
|                            |                                                                                 | the form ``group.topic-name``, e.g., ``gracedb.cbc_gstlal``.               |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
| Controlling subscriptions/ | Subscriptions were added or removed using the ``lvalert subscribe/unsubscribe`` | Permissions are added to a credential manually using SCIMMA's web-based    |
| permissions                | command, or the ``lvalert_admin`` tool.                                         | interface. Note: the process of adding credentials and permissions should  |
|                            |                                                                                 | only have to be done once.                                                 |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+
| Showing nodes/topics       | Available nodes were listed using the ``lvalert nodes`` command, or the         | Available topics are shown for all permitted groups in the SCIMMA web      |
|                            | ``lvalert_admin`` tool.                                                         | interface. Once a permission is added for a topic, then it is listed with  |
|                            |                                                                                 | the ``client.get_topics()`` command.                                       |
+----------------------------+---------------------------------------------------------------------------------+----------------------------------------------------------------------------+

Also removed from igwn-alert are the stand-alone `Python2 executables <https://git.ligo.org/lscsoft/lvalert/-/tree/master/bin>`_
(``lvalert_listen``, ``lvalert_send``, ``lvalert_admin``). As such, the use of ``.ini`` files to 
launch external processes is no longer supported. Users are encouraged to take advantage
of the new Python3 API.

